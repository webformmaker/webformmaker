## Simple web form making

### Quickly and easily customize the layout, colors and style of all forms

Tired of trying web form making?  No worries. We are creating simple and complex forms by quick and easy drag-n-drop form-building method

#### Our features:

* A/B Testing
* Form Conversion
* Form Optimization
* Branch logic
* Payment integration
* Third party integration
* Push notifications
* Multiple language support
* Conditional logic
* Validation rules
* Server rules
* Custom reports

### Our forms will always look great on any desktop and mobile devices
All form elements are made with CSS, without images, so they look sharp, pixel-perfect on all Retina displays and high resolution screens by our [web form maker](https://formtitan.com)

Happy web form making!


